import '../css/style.scss'
import 'slick-carousel'
import 'selectize'
import ViewPort from './modules/module.viewport'
import Tab from './modules/module.tab'
import './modules/module.finance'
import './modules/module.form'
import './modules/module.helper'
import Popup from './modules/module.popup'
import Score from './modules/module.passcomplexity'

import tippy from 'tippy.js';
import objectFitImages from 'object-fit-images';
import './modules/brazzers.js';
import 'sticky-kit/dist/sticky-kit.js'

import $ from "jquery";

import 'lightgallery'
import 'lg-video'

import LazyLoad from 'vanilla-lazyload';


window.app = window.app || {};



if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
	document.documentElement.classList += 'pointer-events';
	window.app.mobile = true;
}
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1 ){
	window.app.ie = true;
}

$(()=>{

	console.info(window.app.mobile);

	if ( !window.app.mobile ){

		import('simplebar').then(module => {
			$('.js-scrollbar').each((i, e)=>{
				new module.default(e);
			});
		});

		$('.aside').stick_in_parent({parent: '.collect__aside', offset_top: 20});
	}

	new Tab();

	window.app.popup = new Popup({
		bodyClass: 'show-popup',
	});

	let $b = $('body'), app = {}, breakpoint;


	objectFitImages('.img-contain');

	let lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy",
		callback_loaded: (el)=>{
			$(el).removeClass('lazy');
			objectFitImages(el);
		}
	});


	new ViewPort({
		'0': ()=>{
			breakpoint = 'mobile';
		},
		'1200': ()=>{
			breakpoint = 'desktop';
		}
	});

	$('select').selectize({
		// sortField: 'field',
		// render: {
		// 	option: function (data, escape) {
		// 		console.info(data, escape);
		// 		return "<div data-comment='" + data.comment + "'>" + data.text + "</div>"
		// 	}
		// },
		onInitialize: function () {
			// let autocomplete = $(this.$input).attr('data-autocomplete');
			//
			// if ( !autocomplete ){
			//
			// 	console.info(this.$control_input);
			// }


			if (!$(this.$input[0]).attr('data-autocomplete')){
				$(this.$control_input).prop('disabled', true);
			}
			if ($(this.$input[0]).attr('data-placeholder')){
				$(this.$control_input).attr('placeholder', $(this.$input[0]).attr('data-placeholder'));
			}

		},
		onFocus: function(e){
			// $(this.$input).closest('.form__field').addClass('f-focus');
		},
		onBlur: function(e){
			// $(this.$input).closest('.form__field').removeClass('f-focus');
		},
		onChange: function(value){
			// console.info(this.revertSettings.$children, $(this.revertSettings.$children).find('option'));
			// console.info(this);

			// let $select = $(this.$input),
			// 	data = this.options[value]
			// ;
			//
			// if ( value !== '' ){
			// 	$select.closest('.form__field').addClass('f-filled');
			// } else {
			// 	$select.closest('.form__field').removeClass('f-filled');
			// }
			//
			// if (data ){
			// 	if (data.comment) $select.closest('.form__group').find('.form__quote').html(data.comment);
			// 	if (data.price) $select.closest('.page__friends-calc').find('.page__friends-calc-price').html(data.price);
			// 	if (data.oldprice) $select.closest('.page__friends-calc').find('.page__friends-calc-oldprice').html(data.oldprice);
			// 	if (data.oneprice) $select.closest('.page__friends-calc').find('.page__friends-calc-saleprice').html(data.oneprice);
			// 	if (data.oneoldprice) $select.closest('.page__friends-calc').find('.page__friends-calc-saleoldprice').html(data.oneoldprice);
			// 	if (data.saving) $select.closest('.page__friends-calc').find('.page__friends-calc-sale').html(data.saving);
			// }

			// console.info(data);
		}
	})
	;


	if (getQueryVariable('show_popup')){
		console.info(getQueryVariable('show_popup'));
		window.app.popup.open(`#${getQueryVariable('show_popup')}`);
	}

	let inputTimer = null;

	$b
		.on('mousedown', (e)=>{
			if ( $('.show-dropdown').length ){
				if ( !$(e.target).closest('.show-dropdown').length ) $('.show-dropdown').removeClass('show-dropdown');
			}
			if ( $('.search-result.is-active').length ){
				if ( !$(e.target).closest('.search').length ) $('.search-result.is-active').removeClass('is-active');
			}
		})
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			let offset = breakpoint === 'mobile' ? 140 : 20;
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - offset}, 500);
		})
		.on('click', '.js-dropdown-open', function(e){
			e.preventDefault();

			let $t = $(this),
				$wrap = $t.closest('.js-dropdown')
			;
			if ( $wrap.hasClass('show-dropdown')){
				$wrap.removeClass('show-dropdown');
			} else {
				$('.show-dropdown').removeClass('show-dropdown');
				$wrap.addClass('show-dropdown');
			}

		})
		.on('click', '.js-cities', function(e){
			e.preventDefault();

			$b.toggleClass('is-show-cities')

		})
		.on('change', '.js-cities-hold', (e)=>{

			if ( e.target.checked ){
				$('.city__content').addClass('is-hold');
			} else {
				$('.city__content').removeClass('is-hold');
			}
		})
		.on('keyup change', '.js-cities-search', (e)=>{
			let $item = $('.city__content').find('[data-city-name]'),
				value = e.target.value.toString().toLowerCase().replace(/ё/g,'е').replace(/й/g,'и')
			;

			if ( value.length ){

				$('.city__content').find('.city__list-group, .city__region-group').addClass('is-hidden');

				$item.each(function () {
					let $t = $(this),
						val = $t.attr('data-city-name').toLowerCase().replace(/ё/g,'е').replace(/й/g,'и')
					;

					if ( val.indexOf(value) >= 0 ){
						$t.closest('label').removeClass('is-hidden');
						$t.closest('.city__list-group, .city__region-group').removeClass('is-hidden');
					} else {
						$t.prop('checked', false).closest('label').addClass('is-hidden');
					}

				})
			} else {
				$item.closest('label').removeClass('is-hidden');
				$('.city__content').find('.city__list-group, .city__region-group').removeClass('is-hidden');
			}

		})
		.on('click', '.city__alphabet-item', function(){
			let $t = $(this),
				word = $t.text().toLowerCase(),
				$wrap = $t.closest('.city__alphabet').parent(),
				$item = $wrap.find('[data-city-name]')
			;

			$t.toggleClass('is-active');
			$t.siblings('.city__alphabet-item').removeClass('is-active');
			$item.removeClass('is-hold');

			$wrap.find('.city__region-group').addClass('is-hold');

			if ( $t.hasClass('is-active')) {
				$item.each(function () {
					let $t = $(this),
						val = $t.attr('data-city-name').toLowerCase()
					;

					if ( val.indexOf(word) === 0 ){
						$t
							.closest('label')
							.removeClass('is-hold')
							.closest('.city__region-group')
							.removeClass('is-hold');
					} else {
						$t.prop('checked', false).closest('label').addClass('is-hold');
					}

				});
			}

		})
		.on('click', '.search-clear', function(){
			$(this).closest('.search').find('input').val('').trigger('change');
			$(this).closest('.search').find('.search-result').removeClass('is-active');
		})
		.on('input change focus', '.search-input', function(){
			let $t = $(this),
				$form = $t.closest('form'),
				val = $t.val().trim(),
				$resultWrap = $t.parent().find('.search-result'),
				$result = $resultWrap.find('.result')
			;

			clearTimeout(inputTimer);

			if (val.length < 3) {
				$result.html('');
				$resultWrap.removeClass('is-active');
				$form.removeClass('loading');
				return;
			}


			inputTimer = setTimeout(()=>{

				if ( val ){
					$form.addClass('loading');
					$.ajax({
						url: $form.attr('action'),
						method: $form.attr('method'),
						data: {query: val, action: 'inputSearch'},
						dataType: 'json',
					}).done((res)=>{
						if ( res.result ){
							$result.html(res.result);
							$resultWrap.addClass('is-active');
						}
					}).fail(()=>{
						$resultWrap.removeClass('is-active');
					}).always(()=>{
						$form.removeClass('loading');
					});
				} else {
					$result.html('');
					$resultWrap.removeClass('is-active');
					$form.removeClass('loading');
				}

			}, 500);


		})

		.on('click', '.aside__menu-toggle', function(){
			let $t = $(this),
				$group = $t.closest('.aside__menu-group'),
				$inner = $group.find('.aside__menu-inner')
			;

			if ( !$group.hasClass('is-buzy') ){
				$group.addClass('is-buzy');

				$inner.slideToggle(300, ()=>{
					$group.toggleClass('is-buzy is-open');
					$inner.removeAttr('style');
				});


			}


		})

		.on('click', '.product__table-toggle', function(){
			let $t = $(this),
				$group = $t.closest('.product__table-value'),
				$inner = $group.find('.product__table-inner')
			;

			if ( !$group.hasClass('is-buzy') ){
				$group.addClass('is-buzy');

				$inner.slideToggle(300, ()=>{
					$group.toggleClass('is-buzy is-open');
					$inner.removeAttr('style');
				});


			}


		})

		.on('click', '.card__filter-view', function(){
			let $t = $(this),
				$wrap = $t.closest('.card'),
				isGrid = $t.hasClass('card__filter-view_grid'),
				isList = $t.hasClass('card__filter-view_list'),
				isShort = $t.hasClass('card__filter-view_short')
			;

			if ( isGrid || isList || isShort ){
				$wrap.removeClass('card_grid card_list card_short');

				if (isGrid) {
					$wrap.addClass('card_grid');
				} else if (isList){
					$wrap.addClass('card_list');
				} else {
					$wrap.addClass('card_short');
				}
			}


		})

		.on('change', ".popup_complaint input[type='radio']", function(){
			$(this).closest('form').find("[type='submit']").prop('disabled', false);
		})
		.on('keyup', "[maxlength]", function(){
			//$(this).closest('form').find("[type='submit']").prop('disabled', false);
			let $t = $(this),
				max = +$t.attr('maxlength'),
				l = $t.val().length,
				$count = $t.closest('.form__field').find('.form__count')
			;

			$count.html(max - l);

		})

		.on('click', ".js-load-phone", function(e){

			let $t = $(this),
				action = $t.attr('data-action'),
				isBtn = $t.hasClass('btn')
			;

			if ( !$t.hasClass('is-loading') && !$t.hasClass('is-done') ){
				e.preventDefault();
				$t.addClass('is-loading');

				$.getJSON(action, (res)=>{
					if ( res.phone ){
						if ( isBtn ){
							$t.html(res.phone).attr('href', `tel:${res.phone}`);
							$t.addClass('is-done');
						} else {
							$t.parent().html(`<a href="tel:${res.phone}">${res.phone}</a>`)
						}
					}
				}).always(function() {
					$t.removeClass('is-loading');
				})
			}


		})

	;


	$('.search').each(function () {
		let $t = $(this),
			$input = $t.find('input'),
			$form = $t.find('form'),
			$clear = $t.find('.header__search-clear'),
			$find = $t.find('.header__search-ico')
		;


		$input.on('change keyup', (e)=>{
			if (e.target.value.trim()) {
				$t.addClass('is-filled');
			} else {
				$t.removeClass('is-filled');
			}
		});

		$clear.on('click', ()=>{
			$input.val('');
		});

		$find.on('click', ()=>{
			if ( $input.val() ) $form.submit();
		});
	});


	$('[data-tooltip-menu]').each((i, e)=>{
		let content = $(e).find('._tooltip-content').html();

		if ( content ){
			tippy(e, {
				content: content,
				delay: [0, 100],
				theme: 'light',
				placement: 'bottom',
				arrow: true,
				interactive: true,
			})
		}
	});


	tippy('[data-tooltip]', {
		content: '',
		theme: 'light',
		arrow: true,
		placement: 'top',
		onShow: (instance)=>{
			instance.setContent($(instance.reference).attr('data-tooltip'));
		}
	});


	$('.js-pass-complesity').each((i, e)=>{
		let $t = $(this),
			baseContent = $(e).parent().find('._tooltip-content').html() || '',
			tooltyp = tippy(e, {
				content: baseContent,
				delay: [0, 100],
				theme: 'light',
				trigger: 'manual',
				placement: 'right',
				arrow: true,
				hideOnClick: false,
				interactive: true,
			}),
			focused = false,
			html = {
				none: `<div class="tooltip-fieldnote"> <p>Пароль должен быть комбинацией из 6 ~ 15 букв (прописные / строчные), цифр и символов без пробелов.</p>
						<p><a href="#" class="js-open-pass-rules">Как подобрать пароль?</a></p></div>`,
				fail: `<div class="tooltip-fieldnote"> <div class="subtitle">Уровень надежности</div>
					<div class="complex">
						<div class="complex__item">
							<div class="complex__value c-red">Непригодный</div>
							<div class="complex__text"></div>
						</div>
					</div>
					<p><a href="#" class="js-open-pass-rules">Как подобрать пароль?</a></p></div>`,
				bad: `<div class="tooltip-fieldnote"> <div class="subtitle">Уровень надежности</div>
					<div class="complex">
						<div class="complex__item">
							<div class="complex__value c-red"><i class="color"></i><i></i><i></i> Слабый</div>
							<div class="complex__text">Пароль не достаточно безопасный</div>
						</div>
					</div>
					<p><a href="#" class="js-open-pass-rules">Как подобрать пароль?</a></p></div>`,
				good: `<div class="tooltip-fieldnote"> <div class="subtitle">Уровень надежности</div>
					<div class="complex">
						<div class="complex__item">
							<div class="complex__value c-yellow"><i class="color"></i><i class="color"></i><i></i> Хороший</div>
							<div class="complex__text">Пароль достаточно безопасный</div>
						</div>
					</div>
					<p><a href="#" class="js-open-pass-rules">Как подобрать пароль?</a></p></div>`,
				best: `<div class="tooltip-fieldnote"> <div class="subtitle">Уровень надежности</div>
					<div class="complex">
						<div class="complex__item">
							<div class="complex__value c-green"><i class="color"></i><i class="color"></i><i class="color"></i> Отличный</div>
							<div class="complex__text">Пароль очень безопасный</div>
						</div>
					</div>
					<p><a href="#" class="js-open-pass-rules">Как подобрать пароль?</a></p></div>`
			};

		// console.info(tooltyp);



		$(e)
			.on('mouseover', function(e){
				// tooltyp.show();
			})
			.on('mouseleave', function(e){
				if (!focused) tooltyp.hide();
			})
			.on('focus', function(e){
				if ( $b.hasClass('show-popup') ){
					focused = true;
					tooltyp.show();
				}
			})
			.on('keyup', function(e){

				let text = '';

				$(this).removeAttr('data-complesity');

				if ( e.target.value.trim() ){
					Score(e.target.value, (data)=>{

						if ( data.score < 40  ){
							text = html.fail;
							$(this).attr('data-complesity', 'Слишком простой пароль');
						} else if ( data.score < 60 ){
							text = html.bad;
						} else if ( data.score < 80 ){
							text = html.good;
						} else {
							text = html.best;
						}
					});
				} else {
					text = html.none;
				}

				if ( $b.hasClass('show-popup') ){
					tooltyp.show();
					tooltyp.setContent(text);
				}

			})
			.on('blur', function(e){
				focused = false;
			})
		;


		$b.on('click', '.js-open-pass-rules', ()=>{
			tooltyp.setContent(baseContent);
		}).on('mousedown', (e)=>{
			if (!$(e.target).closest('.tippy-popper').length ) tooltyp.hide();
		});
	});

	$('.product__image').each((i, t)=>{
		let $wrap = $(t),
			$main = $wrap.find('.product__image-main'),
			$preview = $wrap.find('.product__image-preview')
		;

		$main
			.on('init', function (event, slick) {
				gallery($main, 'a');
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			})
			.on('afterChange', function () {
				lazyLoadInstance.update();
			})
			.slick({
				infinite: false,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				// fade: true,
				adaptiveHeight: true,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
				asNavFor: $preview.length ? $preview : false
			});

		if ( $preview.length ){

			$preview
				.on('afterChange', function () {
					lazyLoadInstance.update();
				})
				.slick({
					speed: 300,
					infinite: true,
					swipe: false,
					swipeToSlide: true,
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: false,
					variableWidth: true,
					focusOnSelect: true,
					asNavFor: $main
				});

		}
	});

	$('[data-slider]').each((i, e)=>{
		let $slider = $(e),
			autoplay = $slider.attr('data-slider-autoplay') || false,
			breackpoint = [1349, 1649],
			options = {
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				autoplay: !!autoplay,
				infinite : !!autoplay,
				autoplaySpeed: autoplay,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				// fade: true,
				adaptiveHeight: true,
				mobileFirst: true,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',

			},
			resp = $slider.attr('data-slider').split(','),
			videoCurrent = null,
			videoAfter = null
		;

		if ( autoplay === 'endvideo' ) {
			options.autoplay = false;
			options.fade = true;
			options.speed = 1000;
			//options.arrows = false;
		} else if ( window.app.ie ){
			options.infinite = false;
		}

		options.slidesToShow = parseInt(resp[0]);

		if (resp.length > 1){
			options.responsive = [];

			for (let i = 1; i < resp.length; i++){
				options.responsive.push({
					breakpoint: breackpoint[i-1],
					settings: {
						slidesToShow: parseInt(resp[i])
					}
				})
			}
		}

		$slider
			.on('init', function (event, slick) {
				if ( autoplay === 'endvideo' ) {
					videoCurrent = slick.$slides[0].getElementsByTagName('video')[0];
					videoCurrent.play();
					videoCurrent.onended = ()=>{
						videoCurrent.play();
						$slider.slick('slickGoTo', 1);
					};
				}

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				if ( autoplay === 'endvideo' ) {
					videoAfter = videoCurrent;
					videoCurrent = slick.$slides[nextSlide].getElementsByTagName('video')[0];
					videoCurrent.play();
					videoCurrent.onended = ()=>{
						videoCurrent.play();
						$slider.slick('slickGoTo', 1);
					};
				}
			})
			.on('afterChange', function (event, slick, currentSlide) {
				if ( autoplay === 'endvideo' ) {
					videoAfter.pause();
				}
				lazyLoadInstance.update();
			})
			.slick(options);

	});

	$('.header__nav-carousel').each((i, e)=>{
		let $slider = $(e),
			options = {
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
				nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M7.5,3.5a1,1,0,0,0-.71.28L.31,9.87a.91.91,0,0,0-.06,1.29l.06.06a1.06,1.06,0,0,0,1.43,0L7.5,5.8l5.77,5.42a1.06,1.06,0,0,0,1.43,0,.92.92,0,0,0,.06-1.29l-.06-.06L8.22,3.78A1,1,0,0,0,7.5,3.5Z"/></svg></div>',
			}
		;


		$slider
			.on('afterChange', function () {
				lazyLoadInstance.update();
			})
			.slick(options);

	});


	function getQueryVariable(variable) {
		let query = window.location.search.substring(1),
			vars = query.split("&")
		;
		for (let i=0;i<vars.length;i++) {
			let pair = vars[i].split("=");
			if(pair[0] === variable){
				return pair[1];
			}
		}
		return(false);
	}



	function gallery(wrap, item) {

		$(wrap).each((i, e)=>{
			$(e).eq(i).lightGallery({
				counter: false,
				download: false,
				loop: true,
				hideControlOnEnd: true,
				selector: item,
				youtubePlayerParams: {
					modestbranding: 1,
					showinfo: 0,
					rel: 0,
					controls: 0
				},
				vimeoPlayerParams: {
					byline : 0,
					portrait : 0,
					color : 'A90707'
				}
			})
		});

	}


	$('.card__item-image-carousel').brazzersCarousel('.card__item-image-item');



	function initMap() {

		$('.js-map').each(function () {
			let $map = $(this),
				$item = $map.find('.js-map-item'),
				data = [],
				placemarks = {},
				center = [0, 0],
				map
			;

			$item.each(function (i) {
				let $t = $(this),
					d = {
						coordinates: $t.attr('data-coordinates').split(',').map(e=>parseFloat(e)),
						html: $t.html(),
						id: i
					}
				;

				data.push(d);

				center[0] = center[0] + d.coordinates[0];
				center[1] = center[1] + d.coordinates[1];

				placemarks[d.id] = new ymaps.Placemark(d.coordinates, {
					balloonContentBody: d.html
				}, {
					iconLayout: 'default#image',
					iconImageHref: '../img/svg/ico_ballun.svg',
					iconImageSize: [20, 26],
					iconImageOffset: [-10, -26]
				});

				$t.remove();

			});

			center[0] = center[0] / data.length;
			center[1] = center[1] / data.length;

			map = new ymaps.Map($map[0], {
				center,
				zoom: 11,
				controls: []
			},{
				maxZoom: 17,
				minZoom: 5,
			});

			for (let i = 0; i < data.length; i++){
				map.geoObjects.add(placemarks[data[i].id]);
			}


			// map.behaviors.disable('scrollZoom');
			map.behaviors.disable('multiTouch');
			// map.behaviors.disable('drag');
			map.setBounds(map.geoObjects.getBounds());


			$('[data-onmap]').click(function (e) {
				e.preventDefault();

				let $t = $(this),
					coord = $t.attr('data-onmap').split(',').map(e=>parseFloat(e))
				;
				$('html,body').animate({scrollTop: $map.offset().top - 30}, 500);
				console.info(coord);
				map.setCenter(coord, 17, {
					duration: 300
				})
			})

		})
	}

	if (window.ymaps) window.ymaps.ready(initMap);





});

