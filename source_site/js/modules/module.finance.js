import $ from 'jquery'
import Highcharts from 'highcharts/highstock';
import 'webticker/jquery.webticker.min.js'


window.app = window.app || {};

$(()=>{

	if ( $('.ticker__wrap').width() > $('.wrapper').width() - 100 ){

		$('.ticker__wrap').webTicker({
			duplicate: true,
			startEmpty: true
		});

		$('.ticker__wrap').webTicker('stop');
		setTimeout(()=>{
			$('.ticker__wrap').webTicker('cont');
		}, 300);
	}


	const $finance = $('.finance');
	const $financeMore = $('.finance__more');


	let dates = {
		month: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
		week: ['пн','вт','ср','чт','пт','сб','вс']
	};

	Highcharts.SVGRenderer.prototype.symbols.customcircle =
		function(x, y, w, h) {
			let r = 6;
			// console.info(x, y, w, h);

			return [
				//
				'M', 0, -x,
				'm', -r, 0,
				'a', r, r, 0, 1, 0, 2 * r, 0,
				'a', r, r, 0, 1, 0, -2 * r, 0,

				'M', -1.5, -x - (r/2),
				'L', -1.5, -x + (r/2),
				'M', 1.5, -x - (r/2),
				'L', 1.5, -x + (r/2),
			];
		};

	if (Highcharts.VMLRenderer) Highcharts.VMLRenderer.prototype.symbols.customcircle = Highcharts.SVGRenderer.prototype.symbols.customcircle;


	const options = {
		xAxis: {

			range: 7 * 24 * 3600 * 1000,

			crosshair:{
				color: '#a2d1ff',
				dashStyle: 'Dash',
				// snap:true
				// width:1
				zIndex:1
			},

		},
		yAxis: {
			gridLineColor: '#a2d1ff',
			opposite: false
		},

		scrollbar: {
			enabled: false
		},

		navigator: {
			handles: {
				backgroundColor: '#3399FF',
				borderColor: '#fff',
				symbols: ['customcircle', 'customcircle'],
				lineWidth: 1,
				width: 15,
				height: 15
			},
			series: {
				color: '#f1f1f1',
				fillOpacity: 1,
				lineWidth: 0,
			},

		},


		lang: {
			months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
			shortMonths: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
			weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
			shortWeekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			printChart: 'Печатать график',
			rangeSelectorZoom: '',
			rangeSelectorFrom: 'От',
			rangeSelectorTo: 'до'
		},

		rangeSelector: {
			enabled: false,
		},

		title: false,

		plotOptions: {

			line: {
				states: {
					hover: {
						lineWidthPlus: 0
					}
				}
			},


			series: {
				lineWidth: 2,
				color: '#FF3300',

				states: {

					hover: {
						halo: {
							opacity: 0,
						}
					}
				},

				marker: {
					states: {
						hover: {
							fillColor: '#fff',
							lineColor: '#FF3300',
							lineWidthPlus: 2,
							radius: 5,
							radiusPlus: 0,
							zIndex: 10
						}
					}
				},

			}
		},

		series: [{
			data: [
				{x: 0,y: 0},
				{x: 100000,y: 0}
			]
		}],

		tooltip: {
			borderWidth: 0,
			backgroundColor: '#fff',
			borderRadius: 4,
			shadow: true,
			style: '{"color": "#000", "cursor": "default", "fontSize": "12px", "pointerEvents": "none", "whiteSpace": "nowrap"}',

			headerFormat: "",
			useHTML: true,
			formatter: function () {
				let date = new Date(this.x);
				return `<div style="font-size: 20px; font-weight: 500;">${this.y}</div>
						<div>${dates.week[date.getDay()]}, ${dates.month[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}</div>`;
			},
		},
	};

	let chart = Highcharts.stockChart($finance.find('.finance__chart-item')[0], options);

	let data = [];

	let timer = null;

	$('body')
		.on('click', '[data-chart]', function () {
			let url = $(this).attr('data-chart');
			getChart(url);
		})
		.on('click', '[data-chart-range]', function () {

			if (!data.length) return false

			let range = $(this).attr('data-chart-range'),
				ms = parseInt(range) * 8.64e+7;

			$(this).addClass('is-active').siblings().removeClass('is-active');

			if ( parseInt(range) === 0 ){
				chart.xAxis[0].setExtremes(data[data.length - 1].x - 8.64e+7, data[data.length - 1].x);
			} else {
				setRange(ms);
			}

		})
		.on('click', '.js-close-finance', function () {
			$finance.slideUp();
		})
		.on('click', '.js-more-finance', function () {
			$finance.slideUp();
			$financeMore.slideDown();
		})
		.on('click', '.js-finance-back', function () {
			$financeMore.slideUp();
			$finance.slideDown();
		})
	;



	$finance.find('.finance__menu').find('select').on('change', function (e) {

		$finance.find('.finance__menu').find('select').not(this).each((i, item) => {
			item.selectize.setValue(0, true);
		});

		getChart($(this).val());
	});


	function getChart(url){

		if ( !url || url === '0' ) return false;

		clearTimeout(timer);
		timer = setTimeout(()=>{

			$financeMore.slideUp();
			$finance.slideDown();

			$.getJSON(url, (res)=>{
				data = res.data;
				chart.series[0].setData(res.data, true);
				$finance.find('.finance__subtitle').html(res.title);

				chart.xAxis[0].setExtremes(data[data.length - 1].x - (7 * 24 * 3600 * 1000), data[data.length - 1].x);

				if ( res.table ){

					let $table = `<table><thead><tr>`;

					for (let i = 0; i < res.table.headers.length; i++){
						$table += `<th>${res.table.headers[i]}</th>`
					}

					$table += `<tr></thead><tbody>`;

					for (let j = 0; j < res.table.data.length; j++){
						$table += `<tr>`;
						for (let k = 0; k < res.table.data[j].length; k++){
							$table += `<td>${res.table.data[j][k]}</td>`;
						}
						$table += `</tr>`;
					}

					$table += `</table>`;

					$finance.find('.finance__list-table').html($table);
				}

			});
		}, 50);

	}


	function setRange(f, t){

		let line = data;

		let from = f === 0 ? line[0].x : t ? new Date(f).getTime() : new Date(chart.xAxis[0].getExtremes().max - f).getTime(),
			to = f === 0 ? line[line.length - 1].x : t ? new Date(t).getTime() : chart.xAxis[0].getExtremes().max
		;

		if ( !t && f !== 0 && to - line[0].x < f ){
			to = line[0].x + f;
		}

		chart.xAxis[0].setExtremes(from, to);

	}

});