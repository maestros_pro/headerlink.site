import $ from 'jquery'


window.app = window.app || {};


export default class Auth {
	constructor(options) {
		Object.assign(this._options = {}, this._default(), options);
		this.$el = {};
		this.$btn = {};
		this.$input = {};
		this.$action = {};
		$(()=>{
			this._init();
		});
	}

	_default() {
		return {

		}
	}

	_showField(el){
		if ( el.prop("tagName") === 'BUTTON'){
			el.removeClass('is-hidden');
		} else {
			if ( el.closest('.form__group').length ){
				el.closest('.form__group').removeClass('is-hidden');
			} else {
				el.closest('.form__field').removeClass('is-hidden');
			}
		}
	}

	_hideField(el){
		if ( el.prop("tagName") === 'BUTTON'){
			el.addClass('is-hidden');
		} else {
			if ( el.closest('.form__group').length ){
				el.closest('.form__group').addClass('is-hidden');
			} else {
				el.closest('.form__field').addClass('is-hidden');
			}
		}
	}

	_sendAjax(data){
		return $.ajax({
			url: this.$el.form.attr('action'),
			method: this.$el.form.attr('method'),
			data,
			dataType: 'json',
		}).done((res)=>{
			if ( res.open_url ){
				window.location.href = res.open_url;
			}
		}).fail(()=>{

		}).always(()=>{

		});
	}

	_getError($el, text){

		if( text ){

			let $field = $el.closest('.form__field'),
				$message = $field.find('.form__message')
			;

			if (!$message.length){
				$message = $('<div class="form__message"></div>').appendTo($field.eq($field.length - 1));
			}

			$field.addClass('f-error');
			$field.addClass('f-message');
			$message.html(text);

		} else {

			$el
				.closest('.form__field')
				.removeClass('f-error f-message')
				.find('.form__message')
				.html('')
			;

		}

	}

	_getTooltip($el, text){
		if( text ){
			$el.closest('.form__field').attr('data-tooltip', text);
		} else {
			$el.closest('.form__field').attr('data-tooltip', '');
		}

	}

	_goToStep(name){

		this._hideField(this.$input.email);
		this._hideField(this.$input.authpassword);
		this._hideField(this.$input.regpassword);
		this._hideField(this.$btn.next);
		this._hideField(this.$btn.login);
		this._hideField(this.$btn.register);
		this._hideField(this.$action.passrecover);

		this.$btn.next.prop('disabled', true);
		this.$btn.login.prop('disabled', true);
		this.$btn.register.prop('disabled', true);

		this._getTooltip(this.$btn.next);

		switch (name){

			case 'login':

				this._showField(this.$input.email);
				this._showField(this.$btn.next);
				this.$btn.next.prop('disabled', true);

				break;

			case 'auth':
				this._showField(this.$input.email);
				this._showField(this.$input.authpassword);
				this._showField(this.$btn.login);
				this._showField(this.$action.passrecover);

				break;

			case 'register':
				this._showField(this.$input.email);
				this._showField(this.$input.regpassword);
				this._showField(this.$btn.register);

				break;

			case 'passrecover':
				window.app.popup.open('#popupCode');
				break;

			case 'newpass':
				window.app.popup.open('#popupNewPass');
				break;

		}

	}

	_collectElements() {
		this.$el.form = $('[data-form=auth]');
		this.$el.codeform = $('[data-form=code]');

		this.$input.email = this.$el.form.find('[data-auth-input=email]');
		this.$input.authpassword = this.$el.form.find('[data-auth-input=authpassword]');
		this.$input.regpassword = this.$el.form.find('[data-auth-input=regpassword]');

		this.$input.code = this.$el.codeform.find('[data-auth-input=code]');

		this.$btn.next = this.$el.form.find('[data-auth-btn=next]');
		this.$btn.login = this.$el.form.find('[data-auth-btn=login]');
		this.$btn.confirm = this.$el.codeform.find('[data-auth-btn=confirm]');
		this.$btn.register = this.$el.form.find('[data-auth-btn=register]');
		this.$btn.savepass = this.$el.form.find('[data-auth-btn=savepass]');

		this.$action.passrecover = this.$el.form.find('[data-auth-action=passrecover]');
		this.$action.codetimer = this.$el.codeform.find('[data-auth-action=codetimer]');
		this.$action.sendcode = this.$el.codeform.find('[data-auth-action=sendcode]');
	}

	_check(){

		setTimeout(()=> {this.$input.email.trigger('change')}, 500);
		setTimeout(()=> {this.$input.authpassword.trigger('change')}, 1000);
	}

	_init() {
		this._collectElements();
		this._goToStep('login');

		let timer = null,
			codeTimeout = null
		;

		window.app.popup.onOpen((e)=>{
			this._goToStep('login');
			this._check();
		});

		this.$input.email.on('keydown input change', ()=>{

			clearTimeout(timer);

			let emailRegex = /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/i,
				val = this.$input.email.val().trim(),
				$dropdown = this.$input.email.closest('.form__input').find('.form__dropdown')
			;

			this.$input.code.closest('.popup').find('.js-emailcode').text(val);

			this.$btn.next.prop('disabled', true);

			$dropdown.removeClass('is-show').html('');
			this._getTooltip(this.$btn.next);

			if ( val.length < 3 ){

				return;
			}

			timer = setTimeout(()=>{

				this._sendAjax({
					action: 'emailCheck',
					email: val
				})
					.done((res)=>{
						this._goToStep('login');
						if ( emailRegex.test(val) ){
							this._goToStep(res.nextStep);
							this._getTooltip(this.$btn.next);
						} else {
							this._getTooltip(this.$btn.next, 'Проверьте корректность email');
						}

						if ( res.list && res.list.length ){
							$dropdown.addClass('is-show');

							res.list.map(item => {
								$dropdown.append(`<div class="form__dropdown-item">${item}</div>`)
							})
						}

						if ( res.comment ){
							this._getTooltip(this.$btn.next, res.comment);
						}

					});
			}, 300)

		});

		this.$input.authpassword.on('keydown input change', ()=>{
			let val = this.$input.authpassword.val().trim();
			if ( !!val ){
				this.$btn.login.prop('disabled', false);
			} else {
				this.$btn.login.prop('disabled', true);
			}
		});

		this.$input.regpassword.on('keydown input change', ()=>{

			setTimeout(()=>{
				let val = this.$input.regpassword.val().trim(),
					complesity = this.$input.regpassword.attr('data-complesity')
				;
				if ( !!val && !complesity ){
					this.$btn.register.prop('disabled', false);
					this._getTooltip(this.$btn.register);
				} else {
					if (complesity ) this._getTooltip(this.$btn.register, complesity);
					this.$btn.register.prop('disabled', true);
				}
			}, 100);
		});

		this.$input.code.on('keydown input change', ()=>{
			this._getError(this.$btn.confirm);

			let val = this.$input.code.val().trim();
			if ( !!val ){
				this.$btn.confirm.prop('disabled', false);
			} else {
				this.$btn.confirm.prop('disabled', true);
			}
		});

		this.$action.passrecover.on('click', (e)=>{
			e.preventDefault();
			let timeout = 0;
			this.$action.sendcode.addClass('disabled');


			this._sendAjax({
				action: 'passRecoveryEmail',
				email: this.$input.email.val().trim() || this.$input.code.closest('.popup').find('.js-emailcode').text()
			})
				.done((res)=>{
					if ( res.status ){
						this._goToStep('passrecover');
						timeout = parseInt(res.timeout);
						this.$action.codetimer.text(timeout);

						codeTimeout = setInterval(()=>{
							timeout--;
							if ( timeout <= 0){
								clearInterval(codeTimeout);
								this.$action.sendcode.removeClass('disabled');
							}
							this.$action.codetimer.text(timeout);
						}, 1000)
					}
				});

		});

		this.$action.sendcode.on('click', ()=>{

			if ( !this.$action.sendcode.hasClass('disabled') ){
				let timeout = 0;
				this.$action.sendcode.addClass('disabled');
				this.$btn.confirm.prop('disabled', true);
				this.$input.code.val('');

				this._sendAjax({
					action: 'passRecoveryEmail',
					email: this.$input.email.val().trim() || this.$input.code.closest('.popup').find('.js-emailcode').text()
				})
					.done((res)=>{
						if ( res.status ){
							timeout = parseInt(res.timeout);
							this.$action.codetimer.text(timeout);

							codeTimeout = setInterval(()=>{
								timeout--;
								if ( timeout <= 0){
									clearInterval(codeTimeout);
									this.$action.sendcode.removeClass('disabled');
								}
								this.$action.codetimer.text(timeout);
							}, 1000)
						}
					});
			}

		});

		this.$btn.confirm.on('click', ()=>{

			this._sendAjax({
				action: 'passRecoveryCode',
				email: this.$input.code.closest('.popup').find('.js-emailcode').text(),
				code: this.$input.code.val().trim()
			})
				.done((res)=>{
					if ( res.status ){
						this._goToStep('newpass');
					} else if (res.error) {
						this._getError(this.$btn.confirm, res.error);
					}
				});
		});

		this.$btn.register.on('click', ()=>{

			this._sendAjax({
				action: 'register',
				email: this.$input.email.val().trim(),
				password: this.$input.regpassword.val().trim(),
			})
				.done((res)=>{
					if ( res.status && !res.open_url ){
						location.reload();
					} else if (res.error) {
						this._getError(this.$input.regpassword, res.error);
					}
				});
		});

		this.$btn.login.on('click', ()=>{

			this._sendAjax({
				action: 'login',
				email: this.$input.email.val().trim(),
				password: this.$input.authpassword.val().trim(),
			})
				.done((res)=>{
					if ( res.status && !res.open_url ){
						location.reload();
					} else if (res.error) {
						this._getError(this.$input.authpassword, res.error);
					}
				});
		});

	}

}