<?php
header('Access-Control-Allow-Origin: *');
sleep(0.5);


$data = array();


$stat = array();
$now = round(microtime(true) * 1000);
$count = 0;

for ($i = 0; $i < 500; $i++) {


	array_push($stat, array(
		"x" => $now - ((500 - $i) * 86400000),
		"y" => $count
	));

	$count = $count + rand(-20, 20);

	if ( $count < 0) $count = -$count;

}

$data['data'] = $stat;
$data['title'] = 'Динамика курса доллара США к рублю (USDTOM_UTS, MOEX)';
$data['table'] = array(
	"headers" => array(
		'Дата',
		'Курс',
		'Изменение',
	),
	"data" => array(
		array(
			'20.03.20',
			77.70321,
			'0.35 <i class="up"></i>',
		),
		array(
			'21.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'22.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'23.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'24.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'25.03.20',
			77.70321,
			'0.35 <i class="up"></i>',
		),
		array(
			'26.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'27.03.20',
			77.70321,
			'-0.35 <i class="down"></i>',
		),
		array(
			'28.03.20',
			77.70321,
			'1.35 <i class="up"></i>',
		),
		array(
			'29.03.20',
			77.70321,
			'-4.35 <i class="down"></i>',
		),
	)
);

echo json_encode($data);
?>