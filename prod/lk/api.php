<?php
//header('Access-Control-Allow-Origin: *');
//sleep(0.5);

ini_set('session.use_cookies', 'On');
ini_set('session.use_trans_sid', 'On');
			
session_set_cookie_params(0, '/', ".headerlink.ru");
define('DIR_DOWNLOAD', '/home/headerlink/public_html/img/');

function api($data) {
	session_start();
	$request = $data;
	$request = http_build_query($request); 
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"http://headerlink.ru/index.php?route=api/headerlink");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
	curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

	// Receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	session_write_close();
	$json = curl_exec($ch);
	$data = json_decode($json);

	curl_close ($ch);
	return $data;
}

$data = array();

function randomDate($start_date, $end_date){
	$min = strtotime($start_date);
	$max = strtotime($end_date) ? strtotime($end_date) : $end_date;

	$val = rand($min, $max);

	return date('Y-m-d', $val);
}

function getUserLogo($id){

	$user = array(
		"logo" => '',
		"name" => ''
	);

	switch ($id % 5){
		case 0:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_bart.jpg';
			$user['name'] = 'Bartalameo Simpson';
			$user['id'] = 1;
			break;
		case 1:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_birns.jpg';
			$user['name'] = 'Montgomery Birns';
			$user['id'] = 2;
			break;
		case 2:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_homer.jpg';
			$user['name'] = 'Homer Simpson';
			$user['id'] = 3;
			break;
		case 3:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_lisa.jpg';
			$user['name'] = 'Lisa Simpson';
			$user['id'] = 4;
			break;
		case 4:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_bender.jpg';
			$user['name'] = 'Bender Rodrigues';
			$user['id'] = 5;
			break;
		default:
			$user['logo'] = 'http://crm.headerlink.maestros.ru/img/dev/user_marge.jpg';
			$user['name'] = 'Margery Simpson';
			$user['id'] = 6;
			break;
	}

	return $user;
}


$action = $_POST['action']; // метод запроса {{string}}

switch ($action) {

	case 'emailCheck':	// проверка email
		$data = api($_POST);
		break;

	case 'passRecoveryEmail':
		$data = api($_POST);
		break;

	case 'passRecoveryCode':
		$data = api($_POST);
		break;

	case 'register':
		$data = api($_POST);
		break;

	case 'login':	// авторизация
		$data = api($_POST);
		break;
		
	case 'filesUpload':	// загрузка файлов

		if (is_uploaded_file($_FILES['file']['tmp_name']) && file_exists($_FILES['file']['tmp_name'])) {
			$filename = md5(mt_rand()) . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				 
			//$json['filename'] = $filename; // . '.' . $ext;
			//$json['mask'] = $filename;
					
			move_uploaded_file($_FILES['file']['tmp_name'], DIR_DOWNLOAD . $filename); // . '.' . $ext);
		}

		header("Location: http://headerlink.ru/api/?action=".$action."&filename=" . $filename);
		exit;
		$data = array();
		$data['id'] = rand(100, 10000);
		//$data['name'] = $_FILES['file']['name'];
		$data['extension'] = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$data['name'] = $_FILES['file']['name'];
		//$data['thumbnail'] = $_FILES['file']['type'];

		break;

	case 'getCurrentUserInfo':
		$data = api($_POST);
		break;
		
	case 'updateUserImage':
		$data = api($_POST);
		break;

	case 'getMetro':
		$data = api($_POST);
		break;
		
	case 'getSphere':
		$data = api($_POST);
		break;
		
	case 'getRegion':
		$data = api($_POST);
		break;

	/*case 'getFormSobstv':	
		$data = api($_POST);
		break;*/

	case 'getFormData':
		$forms = api(array('action'=>'getFormSobstv'));
		$data['companyLegal'] = $forms->sections;

		$data['advertType'] = array (
			array (
				'id' => "1",
				'name' => 'Продам',
			),
			array (
				'id' => "2",
				'name' => 'Куплю',
			),
			array (
				'id' => "3",
				'name' => 'Сдам',
			)
		);

		$data['advertCount'] = array (
			array (
				'id' => "1",
				'name' => 'за единицу',
				'shortname' => 'шт.',
			),
			array (
				'id' => "2",
				'name' => 'за упаковку',
				'shortname' => 'уп.',
			),
			array (
				'id' => "3",
				'name' => 'за пачку',
				'shortname' => 'п.',
			)
		);

		$data['branchType'] = array (
			array (
				'id' => "1",
				'name' => 'Офис',
			),
			array (
				'id' => "2",
				'name' => 'Магазин',
			)
		);
		$settings = api(array('action'=>'getSettings'));
		//$data['settings'] = $settings;
		
		$data['settings'] = array (
			'advertTitleLenght' => $settings->settings->config_advert_limit,
			'newsTitleLenght' => $settings->settings->config_news_limit,
			'articleTitleLenght' => $settings->settings->config_article_limit
		);
		break;

	case 'getAdvertsList':
		$data = api($_POST);
		break;
		
	case 'insertAdvert':
		$request = $_POST;
		foreach($_POST['advert_gallery'] as $key=>$value){
			$request['advert_gallery'][$key] = json_decode($value);
		}
		$data = api($request);
		break;
		
	case 'deleteAdvert':
		$data = api($_POST);
		break;
		
	case 'insertNews':
		$request = $_POST;
		foreach($_POST['news_gallery'] as $key=>$value){
			$request['news_gallery'][$key] = json_decode($value);
		}
		$data = api($request);
		break;
		
	case 'deleteNews':
		$data = api($_POST);
		break;
		
	case 'insertArticle':
		$request = $_POST;
		foreach($_POST['article_gallery'] as $key=>$value){
			$request['article_gallery'][$key] = json_decode($value);
		}
		$data = api($request);
		break;
		
	case 'deleteArticles':
		$data = api($_POST);
		break;

	case 'loadArticle':
		$data = api($_POST);
		break;
		/*$data['article_title'] = 'Какйо то там заголовок';
		$data['article_theme'] = array(979, 858);
		$data['article_text'] = 'вы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывапвы фыв ФЫв ФыЫВП ывп вапывапывап ывап ывапывапвап вап ывапвыапыв апывап';
		$data['article_gallery'] = array(
			array(
				"comment" => '',
				"id" => 5984,
				"rotate" => 0,
				"extension" => 'jpg',
				"thumbnail" => 'https://pp.userapi.com/c850532/v850532402/14557f/MXhhbufHfOM.jpg'
			)
		);
		$data['article_date'] = '25.06.2019';
		$data['article_time'] = '14:18';
		$data['article_publish'] = true;

		break;*/
		
	case 'loadNews':
		$data = api($_POST);
		break;
		
	case 'getShowcaseList':
		// запрос на получение списка витрин
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}


		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(58 * ( $query ? 0.1 : 1));
		$count['archive'] = ceil($count['all'] * 0.10 * ( $query ? 0.01 : 1));
		$count['disabled'] = ceil($count['all'] * 0.20 * ( $query ? 0.01 : 1));
		$count['moderate'] = ceil($count['all'] * 0.01 * ( $query ? 0.01 : 1));
		$count['active'] = $count['all'] - $count['moderate'] - $count['archive'] - $count['disabled'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все объявления (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Активные (".$count['active'].")",
				"id" => "active",
				"count" => $count['active']
			],
			[
				"name" => "Архив (".$count['archive'].")",
				"id" => "archive",
				"count" => $count['archive']
			],
			[
				"name" => "Отмененные (".$count['disabled'].")",
				"id" => "disabled",
				"count" => $count['disabled']
			],
			[
				"name" => "На модерации (".$count['moderate'].")",
				"id" => "moderate",
				"count" => $count['moderate']
			],
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$test_variable_a = rand(0, 1) == 1;
			$test_variable_b = rand(0, 20);

			switch ($group){
				case 'moderate':
					$test_variable_b = 20;
				break;
				case 'disabled':
					$test_variable_b = 0;
				break;
				case 'active':
					$test_variable_b = rand(1, 19);
					$test_variable_a = true;
				break;
				case 'archive':
					$test_variable_b = rand(1, 19);
					$test_variable_a = false;
				break;
			}

			$isDiscard = $test_variable_b == 0;
			$isModerate = $test_variable_b == 20;
			$isActive = $test_variable_a && !$isDiscard && !$isModerate;
			$isArchive = !$test_variable_a && !$isDiscard && !$isModerate;

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"title" => "Title",
					"directions" => array(),
					"url" => "http://metal100.ru/doska-obyavleniy/",
        			"description" => "Description",
					"images" => array(),
					"price" => rand(1, 100) * 1000,
					"views" => rand(50, 500),
					"viewsToday" => rand(0, 50),
					"location" => array(),

					"pass" => array(
						"days" => $test_variable_b,
						"percent" => $test_variable_b * 5,
						"message" => $test_variable_b == 20 ? 'На модерации' : $test_variable_b == 0 ? 'Отклонено модератором' : !$test_variable_a ? 'Снято с публикации' : 'Активно',
					)
				),
				"status" => array(
					"url" => rand(0, 1) == 1,
					"discard" => $isDiscard,
					"moderate" => $isModerate,
					"active" => $isActive,
					"archive" => $isArchive,
				)
			));

			for ($img = 0; $img < rand(0, 10); $img++) {
				array_push($list['items'][$i]['info']['images'], 'http://crm.headerlink.maestros.ru/img/dev/picture_'.rand(0, 9).'.jpg');
			}

			for ($loc = 0; $loc < rand(0, 10); $loc++) {
				array_push($list['items'][$i]['info']['location'], 'Какой то адрес');
			}

			for ($dir = 0; $dir < rand(0, 10); $dir++) {
				array_push($list['items'][$i]['info']['directions'], 'Какое то направление');
			}

		}

		$data['adverts'] = array(
			"free" => 400,
			"buzy" => 100,
		);

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;

	case 'getHeadingsList':
		// запрос на получение списка заголовков
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(258 * ( $query ? 0.01 : 1));
		$count['ownInSale'] = ceil($count['all'] * 0.20 * ( $query ? 0.01 : 1));
		$count['ownInRent'] = ceil($count['all'] * 0.20 * ( $query ? 0.01 : 1));
		$count['rent'] = ceil($count['all'] * 0.2 * ( $query ? 0.01 : 1));
		$count['track'] = $count['all'] - $count['ownInSale'] - $count['ownInRent'] - $count['rent'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все заголовки (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "На продаже (".$count['ownInSale'].")",
				"id" => "ownInSale",
				"count" => $count['ownInSale']
			],
			[
				"name" => "В аренде (".$count['ownInRent'].")",
				"id" => "ownInRent",
				"count" => $count['ownInRent']
			],
			[
				"name" => "Арендуемые (".$count['rent'].")",
				"id" => "rent",
				"count" => $count['rent']
			],
			[
				"name" => "Отслеживаемые (".$count['track'].")",
				"id" => "track",
				"count" => $count['track']
			],
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$test_variable_a = rand(0, 1) == 1;
			$test_variable_b = rand(0, 20);
			$test_variable_c = rand(0, 2) == 1;
			$test_variable_d = array();
			$test_variable_e = array();

			for ($tvd = 0; $tvd < rand(0, 2); $tvd++) {
				$text = '';
				switch (rand(0, 3)){
					case 0:
						$text = 'Используется';
						break;
					case 1:
						$text = 'Не используется';
						break;
					case 2:
						$text = 'В продаже';
						break;
					default:
						$text = 'В аренде';
						break;
				}
				array_push($test_variable_d, $text);
			}

			for ($tve = 0; $tve < rand(0, 2); $tve++) {
				$text = '';
				switch (rand(0, 1)){
					case 0:
						$text = 'Используется';
						break;
					default:
						$text = 'Не используется';
						break;
				}
				array_push($test_variable_e, $text);
			}

			$isOwn = rand(0, 1) == 1;
			$isTrack = rand(0, 1) == 1 && !$isOwn;
			$isRent= !$isTrack && !$isOwn;
			$isOwnInSale = rand(0, 1) == 1 && $isOwn;
			$isOwnInRent = rand(0, 1) == 1 && $isOwn && !$isOwnInSale;

			switch ($group){
				case 'ownInSale':
					$isOwnInSale = true;
					$isOwn = true;
					break;
				case 'ownInRent':
					$isOwnInRent = true;
					$isOwn = true;
					$isOwnInSale = false;
					break;
				case 'rent':
					$isRent = true;
					$isTrack = false;
					$isOwn = false;
					break;
				case 'track':
					$isTrack = true;
					$isOwn = false;
					$isRent = false;
					break;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"title" => "Title",
					"priceSelf" => rand(1, 100) * 100,
					"price" => rand(1, 100) * 100,
					"priceInMonth" => rand(-900, 900),
					"rate" => rand(100, 5000),
					"popularity" => rand(-100, 100),
					"state" => $test_variable_c ? $test_variable_e : rand(0, 1) == 1 ? $test_variable_d : [],

					"pass" => $test_variable_c ? array(
							"days" => $test_variable_b,
							"percent" => $test_variable_b * 5,
							"message" => "Арендован / осталось " . $test_variable_b . " дней",
						) : false
				),
				// статус объекта
				"status" => array(
					"own" => $isOwn,				// собственный заголовок
					"ownInSale" => $isOwnInSale,	// собственный на продаже
					"ownInRent" => $isOwnInRent,	// собственный в аренде
					"rent" => $isRent,				// Арендуемый заголовок
					"track" => $isTrack				// Отслеживаемый заголовок
				),
				// возможные действия с объектом
				"event" => array(
					"sale" => $isOwn,													// ПРОДАТЬ
					"buy" => !$isOwn && rand(0, 3) == 1,								// КУПИТЬ
					"getRent" => !$isOwn && !$isRent && !$isTrack && rand(0, 1) == 1,	// ВЗЯТЬ В АРЕНДУ
					"setRent" => $isOwn && rand(0, 3) == 1,								// СДАТЬ В АРЕНДУ
					"endRent" => $isRent,												// ОТМЕНИТЬ АРЕНДУ
					"changeСonditions" => $isOwn && rand(0, 5) == 1,					// ИЗМЕНИТЬ УСЛОВИЯ
				)
			));

		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;

	case 'getDialogsList':
		// запрос на получение списка заголовков
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(100 * ( $query ? 0.01 : 1));
		$count['favorite'] = ceil($count['all'] * 0.22 * ( $query ? 0.01 : 1));
		$count['archive'] = ceil(16 * ( $query ? 0.01 : 1));
//		$count['archive'] = $count['all'] - $count['favorite'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все диалоги (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Важные (".$count['favorite'].")",
				"id" => "favorite",
				"count" => $count['favorite']
			],
			[
				"name" => "Архив (".$count['archive'].")",
				"id" => "archive",
				"count" => $count['archive']
			]
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$isFavorite = rand(0, 3) == 1;
			$isArchive = rand(0, 3) == 1;
			$isGroupDialog = rand(0, 2) == 1;
			$isOnline = rand(0, 2) == 1;
			$membersLength = $isGroupDialog ? rand(3, 30) : false;
			$isUnreadMessage = rand(0, 2) == 1;
			$title = 'Заголовок диалога';
			$logo = array();

			switch ($group){
				case 'favorite':
					$isFavorite = true;
					break;
				case 'archive':
					$isArchive = true;
					break;
			}


			for ($uig = 0; $uig < ($membersLength ? 2 : 1); $uig++){
				$user = getUserLogo(rand(0, 5));
				array_push($logo, $user['logo']);
				$title = $user['name'];
			}


			array_push($list['items'], array(
				"id" => $offset + $i + 1,
				"info" => array(
					"title" => $title,
					"members" => $membersLength,	// если групповой диалог, то колчиество участников, если нет, то false
					"description" => rand(0, 3) == 1 ? 'Вложение' : 'Часть какого-то сообщения из диалога, которое показывается в списке диалогов',
					"logo" => $logo,
					"date" => randomDate('2017-12-10', time()),
					"unread" => $isUnreadMessage ? rand(1, 10) : $isUnreadMessage,	// количество непрочитанных сообщений из диалога
					"unview" => rand(1, 2) == 1	// если сообщение еще не прочитано собеседником
				),
				// статус объекта
				"status" => array(
					"online" => $isOnline,
					"archive" => $isArchive,			// в архиве
					"favorite" => $isFavorite,			// в избранных
				)
			));

		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;

	case 'getBranchesList':
		$data = api($_POST);
		break;
		/*// запрос на получение списка филиалов
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(258 * ( $query ? 0.01 : 1));
		$count['active'] = ceil($count['all'] * 0.78 * ( $query ? 0.01 : 1));
		$count['archive'] = $count['all'] - $count['active'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все филиалы (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Активные (".$count['active'].")",
				"id" => "active",
				"count" => $count['active']
			],
			[
				"name" => "Архив (".$count['archive'].")",
				"id" => "archive",
				"count" => $count['archive']
			]
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$test_variable_a = '';

			switch (rand(0, 2)){
				case 0:
					$test_variable_a = 'Офис';
					break;
				case 1:
					$test_variable_a = 'Магазин';
					break;
				default:
					$test_variable_a = 'Склад';
					break;
			}

			$isArchive = rand(0, 2) == 1;
			$isActive = !$isArchive;

			switch ($group){
				case 'active':
					$isArchive = false;
					$isActive = true;
					break;
				case 'archive':
					$isArchive = true;
					$isActive = false;
					break;
			}

			array_push($list['items'], array(
				"id" => $offset + $i + 1,
				"info" => array(
					"type" => $test_variable_a,
					"name" => $test_variable_a . " Название объекта ",
					"address" => "Какой то адрес",
					"contact" => array()
				),
				// статус филиала
				"status" => array(
					"archive" => $isArchive,
					"active" => $isActive,
				),

			));


			for ($cntc = 0; $cntc < rand(0, 5); $cntc++) {
				array_push($list['items'][$i]['info']['contact'], '8 (123) 456-78-90');
			}

		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];


		break;*/

	case 'getNotificationsList':
		// запрос на получение списка оповещений
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil( 258 * ( $query ? 0.01 : 1));
		$count['mine'] = ceil($count['all'] * 0.78 * ( $query ? 0.01 : 1));
		$count['spam'] = $count['all'] - $count['mine'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все оповещения (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Мои оповещения (".$count['mine'].")",
				"id" => "mine",
				"count" => $count['mine']
			],
			[
				"name" => "Спам (".$count['spam'].")",
				"id" => "spam",
				"count" => $count['spam']
			]
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {


			$isSpam = rand(0, 2) == 1;
			$isMine = !$isSpam;

			switch ($group){
				case 'mine':
					$isSpam = false;
					$isMine = true;
					break;
				case 'spam':
					$isSpam = true;
					$isMine = false;
					break;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"photo" => "http://crm.headerlink.maestros.ru/img/dev/logo_".rand(0, 5).".jpg",
					"title" => "Какой то заголовок",
					"date" => randomDate('2017-12-10', time())
				),
				// статус филиала
				"status" => array(
					"spam" => $isSpam,
					"mine" => $isMine,
				),

			));

		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];


		break;

	case 'getNewsList':
		// запрос на получение списка новостей
		$data = api($_POST);
	break;
		/*$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(2547  * ( $query ? 0.01 : 1));
		$count['announced'] = ceil($count['all'] * 0.10 * ( $query ? 0.01 : 1));
		$count['archive'] = ceil($count['all'] * 0.78 * ( $query ? 0.01 : 1));
		$count['moderate'] = ceil($count['all'] * 0.05 * ( $query ? 0.01 : 1));
		$count['discard'] = ceil($count['all'] * 0.1 * ( $query ? 0.01 : 1));
		$count['active'] = $count['all'] - $count['announced'] - $count['archive'] - $count['moderate'] - $count['discard'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все новости (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Анонсированные (".$count['announced'].")",
				"id" => "announced",
				"count" => $count['announced']
			],
			[
				"name" => "Прошедшие (".$count['archive'].")",
				"id" => "archive",
				"count" => $count['archive']
			],
			[
				"name" => "Отмененные (".$count['discard'].")",
				"id" => "discard",
				"count" => $count['discard']
			],
			[
				"name" => "На проверке (".$count['moderate'].")",
				"id" => "moderate",
				"count" => $count['moderate']
			],
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$isDiscard = rand(0, 4) == 1;
			$isModerate = rand(0, 2) == 1 && !$isDiscard;
			$isArchive= !$isDiscard && !$isDiscard && rand(0, 2) == 1;
			$isActive = !$isDiscard && !$isModerate && !$isArchive;
			$isAnnounced = rand(0, 2) == 1 && $isActive;

			switch ($group){
				case 'announced':
					$isAnnounced = true;
					$isActive = true;
					$isDiscard = false;
					$isArchive = false;
					break;
				case 'archive':
					$isArchive = true;
					$isModerate = false;
					$isActive = false;
					$isAnnounced = false;
					break;
				case 'discard':
					$isDiscard = true;
					$isArchive = false;
					$isActive = false;
					break;
				case 'moderate':
					$isModerate = true;
					$isDiscard = false;
					$isArchive = false;
					break;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"title" => "Title",
					"description" => "Description fugiat ad qui voluptate aliquip excepteur dolore laboris officia laborum culpa aute cupidatat culpa cupidatat id dolor commodo excepteur velit excepteur ea Lorem deserunt do esse deserunt ad officia ullamco exercitation mollit deserunt ex sit et duis sunt Lorem amet",
					"images" => array(),
					"views" => rand(100, 5000),
					"comments" => rand(0, 100),
					"publish" => randomDate('2009-12-10', time()),
					"status" => 'Опубликовано'
				),
				// статус новости
				"status" => array(
					"discard" => $isDiscard,		// Отмененная новость
					"moderate" => $isModerate,		// новость на проверке
					"archive" => $isArchive,		// Прошедшая новость
					"active" => $isActive,			// активная новость
					"announced" => $isAnnounced		// Анонсированная новость
				)
			));


			if ($isDiscard){
				$list['items'][$i]['info']['status'] = 'Отклонено модератором';
			} elseif ($isAnnounced ){
				$list['items'][$i]['info']['status'] = 'Опубликуется завтра в 10:00';
			} elseif ($isArchive ){
				$list['items'][$i]['info']['status'] = 'В архиве';
			} elseif ($isModerate){
				$list['items'][$i]['info']['status'] = 'На проверке у модератора';
			}


			for ($img = 0; $img < rand(0, 10); $img++) {
				array_push($list['items'][$i]['info']['images'], 'http://crm.headerlink.maestros.ru/img/dev/picture_'.rand(0, 9).'.jpg');
			}
		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;*/

	case 'getArticlesList':
		// запрос на получение списка статей
		$data = api($_POST);
	break;
		/*$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(1542 * ( $query ? 0.01 : 1));
		$count['announced'] = ceil($count['all'] * 0.10 * ( $query ? 0.01 : 1));
		$count['archive'] = ceil($count['all'] * 0.78 * ( $query ? 0.01 : 1));
		$count['moderate'] = ceil($count['all'] * 0.05 * ( $query ? 0.01 : 1));
		$count['discard'] = ceil($count['all'] * 0.1 * ( $query ? 0.01 : 1));
		$count['active'] = $count['all'] - $count['announced'] - $count['archive'] - $count['moderate'] - $count['discard'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все статьи (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Анонсированные (".$count['announced'].")",
				"id" => "announced",
				"count" => $count['announced']
			],
			[
				"name" => "Прошедшие (".$count['archive'].")",
				"id" => "archive",
				"count" => $count['archive']
			],
			[
				"name" => "Отмененные (".$count['discard'].")",
				"id" => "discard",
				"count" => $count['discard']
			],
			[
				"name" => "На проверке (".$count['moderate'].")",
				"id" => "moderate",
				"count" => $count['moderate']
			],
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			$isDiscard = rand(0, 4) == 1;
			$isModerate = rand(0, 2) == 1 && !$isDiscard;
			$isArchive= !$isDiscard && !$isDiscard && rand(0, 2) == 1;
			$isActive = !$isDiscard && !$isModerate && !$isArchive;
			$isAnnounced = rand(0, 2) == 1 && $isActive;

			switch ($group){
				case 'announced':
					$isAnnounced = true;
					$isActive = true;
					$isDiscard = false;
					$isArchive = false;
					break;
				case 'archive':
					$isArchive = true;
					$isModerate = false;
					$isActive = false;
					$isAnnounced = false;
					break;
				case 'discard':
					$isDiscard = true;
					$isArchive = false;
					$isActive = false;
					break;
				case 'moderate':
					$isModerate = true;
					$isDiscard = false;
					$isArchive = false;
					break;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"title" => "Title",
					"description" => "Description fugiat ad qui voluptate aliquip excepteur dolore laboris officia laborum culpa aute cupidatat culpa cupidatat id dolor commodo excepteur velit excepteur ea Lorem deserunt do esse deserunt ad officia ullamco exercitation mollit deserunt ex sit et duis sunt Lorem amet",
					"images" => array(),
					"views" => rand(100, 5000),
					"comments" => rand(0, 100),
					"publish" => randomDate('2009-12-10', time()),
					"status" => 'Опубликовано'
				),
				// статус новости
				"status" => array(
					"discard" => $isDiscard,		// Отмененная новость
					"moderate" => $isModerate,		// новость на проверке
					"archive" => $isArchive,		// Прошедшая новость
					"active" => $isActive,			// активная новость
					"announced" => $isAnnounced		// Анонсированная новость
				)
			));

			if ($isDiscard){
				$list['items'][$i]['info']['status'] = 'Отклонено модератором';
			} elseif ($isAnnounced ){
				$list['items'][$i]['info']['status'] = 'Опубликуется завтра в 10:00';
			} elseif ($isArchive ){
				$list['items'][$i]['info']['status'] = 'В архиве';
			} elseif ($isModerate){
				$list['items'][$i]['info']['status'] = 'На проверке у модератора';
			}


			for ($img = 0; $img < rand(0, 10); $img++) {
				array_push($list['items'][$i]['info']['images'], 'http://crm.headerlink.maestros.ru/img/dev/picture_'.rand(0, 9).'.jpg');
			}
		}

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;*/

	case 'getEmployeesList':
	case 'getEmployeesFavorite':
	// запрос на получение списка сотрудников
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(134 * ( $query ? 0.01 : 1));
		$count['fired'] = ceil($count['all'] * 0.17 * ( $query ? 0.01 : 1));
		$count['follow'] = ceil($count['all'] * 0.05 * ( $query ? 0.01 : 1));
		$count['invite'] = $count['all'] - $count['fired'] - $count['follow'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все сотрудники (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Уволенные (".$count['fired'].")",
				"id" => "fired",
				"count" => $count['fired']
			],
			[
				"name" => "Запросы (".$count['follow'].")",
				"id" => "follow",
				"count" => $count['follow']
			],
			[
				"name" => "Приглашения (".$count['invite'].")",
				"id" => "invite",
				"count" => $count['invite']
			],
		);


		// items

		$list = array();
		$list['items'] = array();
		$list['favorites'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		$online = 0;

		if ( $action == 'getEmployeesFavorite' ){
			$pageLimit = 10;
		}

		for ($i = 0; $i < $pageLimit; $i++) {

			$test_variable_a = rand(0, 2) == 1;
			$test_variable_b = randomDate('2017-12-10', time());

			$isOnline = $test_variable_a ? $test_variable_a : $test_variable_b;
			$isFavorite = rand(0, 2) == 1;
			$isInvite = rand(0, 7) == 1;
			$isFollow = rand(0, 2) == 1 && !$isInvite;
			$isFired = rand(0, 1) == 1 && !$isFollow && !$isInvite;

			$employeePhoto = '';
			$employeeName = '';

			if ( $isOnline ) {
				$online++;
			}

			switch (rand(0, 5)){
				case 0:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_bart.jpg';
					$employeeName = 'Bartalameo Simpson';
					break;
				case 1:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_birns.jpg';
					$employeeName = 'Montgomery Birns';
					break;
				case 2:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_homer.jpg';
					$employeeName = 'Homer Simpson';
					break;
				case 3:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_lisa.jpg';
					$employeeName = 'Lisa Simpson';
					break;
				case 4:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_bender.jpg';
					$employeeName = 'Bender Rodrigues';
					break;
				default:
					$employeePhoto = 'http://crm.headerlink.maestros.ru/img/dev/user_marge.jpg';
					$employeeName = 'Margery Simpson';
					break;
			}

			switch ($group){
				case 'fired':
					$isFired = true;
					$isFollow = false;
					$isInvite = false;
					break;
				case 'follow':
					$isFollow = true;
					$isInvite = false;
					$isFired = false;
					break;
				case 'invite':
					$isInvite = true;
					$isFired = false;
					break;
			}

			if ( $action == 'getEmployeesFavorite' ){
				$isFavorite = true;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"photo" => $employeePhoto,
					"position" => array(),
					"contact" => array(),
					"name" => $employeeName
				),
				// статус новости
				"status" => array(
					"online" => $isOnline,		// Работник онлайн
					"favorite" => $isFavorite,	// Работник в избранных
					"invite" => $isInvite,		// Работник приглашен
					"follow" => $isFollow,		// Работник отслеживает
					"fired" => $isFired,			// Работник уволен
					"deleted" => $isFired		// Работник уволен
				)
			));


			for ($pst = 0; $pst < rand(0, 3); $pst++) {
				array_push($list['items'][$i]['info']['position'], 'Должность '.($pst + 1));
			}

			for ($cnt = 0; $cnt < rand(0, 5); $cnt++) {
				array_push($list['items'][$i]['info']['contact'], '8 123 456-78-90');
			}
		}

		$data['online'] = $online;

	// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

	if ( $query && strlen($query) > 9 ) $data['list'] = [];

	break;

	case 'getNotebookList':
	case 'getNotebookFavorite':
	// запрос на получение списка записной книжки
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(57 * ( $query ? 0.01 : 1));
		$count['favorite'] = ceil($count['all'] * 0.17 * ( $query ? 0.01 : 1));
		$count['deleted'] = ceil($count['all'] * 0.05 * ( $query ? 0.01 : 1));
		$count['black'] = $count['all'] - $count['favorite'] - $count['deleted'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все компании (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Избранные (".$count['favorite'].")",
				"id" => "favorite",
				"count" => $count['favorite']
			],
			[
				"name" => "Удаленные (".$count['deleted'].")",
				"id" => "deleted",
				"count" => $count['deleted']
			],
			[
				"name" => "Черный список (".$count['black'].")",
				"id" => "black",
				"count" => $count['black']
			],
		);


		// items

		$list = array();
		$list['items'] = array();
		$list['favorites'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		if ( $action == 'getNotebookFavorite' ){
			$pageLimit = 10;
		}

		for ($i = 0; $i < $pageLimit; $i++) {

			$isFavorite = rand(0, 2) == 1;
			$isDeleted = rand(0, 2) == 1;
			$isBlacklist = rand(0, 3) == 1 && !$isDeleted;


			switch ($group){
				case 'favorite':
					$isFavorite = true;
					break;
				case 'deleted':
					$isDeleted = true;
					$isBlacklist = false;
					break;
				case 'black':
					$isBlacklist = true;
					$isDeleted = false;
					break;
			}

			if ( $action == 'getNotebookFavorite' ){
				$isFavorite = true;
			}

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"photo" => 'http://crm.headerlink.maestros.ru/img/dev/logo_'. rand(0,5).'.jpg',
					"location" => 'Город, Адрес',
					"contact" => array(),
					"name" => 'Название компании',
					"inn" => rand(1000000000, 9000000000)
				),
				// статус объекта
				"status" => array(
					"favorite" => $isFavorite,
					"deleted" => $isDeleted,
					"blacklist" => $isBlacklist
				)
			));



			for ($cnt = 0; $cnt < rand(0, 5); $cnt++) {
				array_push($list['items'][$i]['info']['contact'], '8 123 456-78-90');
			}
		}

	// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

	if ( $query && strlen($query) > 9 ) $data['list'] = [];

	break;

	case 'getBalanceList':
		// запрос на получение списка баланса
		$offset = isset($_POST['offset']) ? $_POST['offset'] : 0; // сдвиг массива коллекции {{number}}
		$limit = isset($_POST['limit']) ? $_POST['limit'] : 10; // длинна массива коллекции {{number}}
		$group = isset($_POST['group']) ? $_POST['group'] : false; // id группы коллекции {{string/number}}
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$data['currentGroup'] = $group;

		if ( $group === false ) $group = 'all';

		// group
		$count = array();
		$count['all'] = ceil(8504 * ( $query ? 0.01 : 1));
		$count['credit'] = ceil($count['all'] * 0.78 * ( $query ? 0.01 : 1));
		$count['debit'] = $count['all'] - $count['active'];

		$data['count'] = $count[$group];

		$data['groups'] = array(
			[
				"name" => "Все филиалы (".$count['all'].")",
				"id" => "all",
				"count" => $count['all']
			],
			[
				"name" => "Приход (".$count['credit'].")",
				"id" => "credit",
				"count" => $count['credit']
			],
			[
				"name" => "Расход (".$count['debit'].")",
				"id" => "debit",
				"count" => $count['debit']
			]
		);


		// items

		$list = array();
		$list['items'] = array();

		$pageLimit = $data['count'] < $limit ? $data['count'] : $data['count'] < $offset + $limit ? $data['count'] - $offset : $limit;

		for ($i = 0; $i < $pageLimit; $i++) {

			if ( $group == 'credit' ){
				$randome = rand(10, 210);
			} elseif ( $group == 'debit' ){
				$randome = rand(-100, -10);
			} else {
				$randome = rand(-100, 210);
			}

			$test_variable_a = $randome * 100;
			$isCredit = $test_variable_a >= 0;
			$isDebit = $test_variable_a < 0;

			array_push($list['items'], array(
				"id" => $offset + $i,
				"info" => array(
					"price" => $test_variable_a,
					"title" => $test_variable_a >= 0 ? 'Пополнение баланса  <a href="/">VIZA 4325</a>' : 'Покупка заголовка <a href="/">"Купить трактор"</a>' ,
					"date" => randomDate('2017-12-10', time()),
				),

				"status" => array(
					"credit" => $isCredit,
					"debit" => $isDebit,
				),

			));

		}

		$data['balance'] = array(
			"available" => 614500,
			"credit" => 1115000,
			"debit" => 3000,
		);

		// если объектов еще нет в БД, то $data['list'] = false,
		$data['list'] = $list['items'];

		if ( $query && strlen($query) > 9 ) $data['list'] = [];

		break;

	case 'getBalancePayments':
		// запрос на получение списка систем оплаты

		$data['payments'] =  array(
			array(
				"id" => "1",
				"logo" => "/img/payment/logo-apple-pay.png",
				"name" => "Apple Pay",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "2",
				"logo" => "/img/payment/logo-bitcoin.png",
				"name" => "Bitcoin",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "3",
				"logo" => "/img/payment/logo-free-kassa.png",
				"name" => "Free-kassa",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "4",
				"logo" => "/img/payment/logo-google-pay.png",
				"name" => "Google Pay",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "5",
				"logo" => "/img/payment/logo-mastercard.png",
				"name" => "MasterCard",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "6",
				"logo" => "/img/payment/logo-mir.png",
				"name" => "Мир",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "7",
				"logo" => "/img/payment/logo-payeer.png",
				"name" => "Payeer",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "8",
				"logo" => "/img/payment/logo-payoneer.png",
				"name" => "Payoneer",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "9",
				"logo" => "/img/payment/logo-paypal.png",
				"name" => "PayPal",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "10",
				"logo" => "/img/payment/logo-payza.png",
				"name" => "payza",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "11",
				"logo" => "/img/payment/logo-qiwi-koshelek.png",
				"name" => "QIWI Кошелек",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "12",
				"logo" => "/img/payment/logo-visa.png",
				"name" => "Visa",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "13",
				"logo" => "/img/payment/logo-webmoney.png",
				"name" => "WebMoney",
				"number" => "**** **** **** ****"
			),
			array(
				"id" => "14",
				"logo" => "/img/payment/logo-yandex-money.png",
				"name" => "Яндекс.Деньги",
				"number" => "**** **** **** ****"
			)
		);

		break;

	case 'getContactList':
		// запрос на получение списка контактов при добавлении в записную книжку

		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$list = array();
		$pageLimit = $query ? 5 : 30;


		for ($i = 0; $i < $pageLimit; $i++) {

			array_push($list, array(
				"id" => $i + 1,
				"info" => array(
					"photo" => 'http://crm.headerlink.maestros.ru/img/dev/logo_'. rand(0,5).'.jpg',
					"location" => 'Город, Адрес',
					"contact" => array(),
					"name" => 'Название компании',
					"inn" => rand(1000000000, 9000000000)
				),

			));


			for ($cnt = 0; $cnt < rand(0, 5); $cnt++) {
				array_push($list[$i]['info']['contact'], '8 123 456-78-90');
			}
		}

		$data['list'] = $list;

		break;

	case 'findHeadings':
		// поиск заголовков
		$query = isset($_POST['query']) ? $_POST['query'] : false; // значение при поиске через поисковую строку {{string}}

		$list = array(
			array(
				"title" => 'ВАШИ СВОБОДНЫЕ ЗАГОЛОВКИ',
				"items" => array()
			),
			array(
				"title" => 'ПРОДАЮЩИЕСЯ ИЛИ В АРЕНДЕ',
				"items" => array()
			),
			array(
				"title" => 'СВОБОДНЫЕ ЗАГОЛОВКИ',
				"items" => array()
			)
		);

		for ($mine = 0; $mine < rand(1, 50); $mine++) {
			array_push($list[0]['items'], array(
				'id' => $mine + 100,
				'name' => '<b>'. $query .'</b> тра ля ля',
				'value' => ''. $query .' тра ля ля',
				'rate' => rand(50, 500000),
				'rateComment' => 'Какой то комментарий',
				'price' => '',
				'action' => 'use'	// use, buy, rent - согласно макетам
			));
		}

		for ($bought = 0; $bought < rand(1, 50); $bought++) {
			array_push($list[1]['items'], array(
				'id' => $bought + 200,
				'name' => '<b>'. $query .'</b> тро ло ло',
				'rate' => rand(50, 500000),
				'rateComment' => 'Какой то комментарий',
				'price' => '<b>4 500 ₽/</b>мес.',
				'action' => rand(0, 1) == 1 ? 'buy' : 'rent'	// use, buy, rent - согласно макетам
			));
		}

		for ($free = 0; $free < rand(1, 50); $free++) {
			array_push($list[2]['items'], array(
				'id' => $free + 300,
				'name' => '<b>'. $query .'</b> трам пам пам',
				'rate' => rand(50, 500000),
				'rateComment' => 'Какой то комментарий',
				'price' => '',
				'action' => 'use'	// use, buy, rent - согласно макетам
			));
		}
		$data['list'] = $list;

		break;

	case 'getNotificationSetting':
		// запрос настроек оповещение
		$list = array(
			array(
				"title" => 'Сообщения',
				"items" => array(
					array(
						'name' => 'От сотрудников',
						'id' => 1,
						'tooltip' => 'Какая то подсказка',
						'active' => false
					),
					array(
						'name' => 'От компаний',
						'id' => 2,
						'tooltip' => 'Какая то подсказка',
						'active' => true,
					),
					array(
						'name' => 'От Headerlink',
						'id' => 3,
						'tooltip' => 'Какая то подсказка',
						'active' => true,
						'default' => true
					),
				)
			),
			array(
				"title" => 'Заголовки',
				"items" => array(
					array(
						'name' => 'Появление на бирже',
						'id' => 4,
						'tooltip' => 'Какая то подсказка',
					),
					array(
						'name' => 'Изменение цены',
						'id' => 5,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 6,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 7,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 8,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 9,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 10,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 11,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 12,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 13,
						'active' => true
					),
					array(
						'name' => 'Смена владельца',
						'id' => 14,
						'active' => true
					),
				)
			)
		);




		$data['list'] = $list;

		break;

	case 'getKitSetting':
		// запрос настроек разделов на главном экране
		$list = array(
			array(
				'name' => 'О компании',
				'id' => 1,
				'active' => true,
				'default' => true,
			),
			array(
				'name' => 'Объявления',
				'id' => 2,
				'active' => true,
			),
			array(
				'name' => 'Статьи',
				'id' => 3,
				'active' => true,
			),
			array(
				'name' => 'Новости',
				'id' => 4,
				'active' => true,
			),
			array(
				'name' => 'Блог',
				'id' => 5,
				'active' => true,
			),
			array(
				'name' => 'Подписчики',
				'id' => 6,
				'active' => true,
			),
			array(
				'name' => 'Объявления',
				'id' => 7,
				'active' => true,
			),
			array(
				'name' => 'Подписки',
				'id' => 8,
				'active' => true,
			),
			array(
				'name' => 'Мои сотрудники',
				'id' => 9,
				'active' => true,
			),
			array(
				'name' => 'Статистика',
				'id' => 10,
				'active' => true,
			),
			array(
				'name' => 'Оповещения',
				'id' => 11,
				'active' => true,
			),
			array(
				'name' => 'Заголовки',
				'id' => 12,
				'active' => true,
			),
			array(
				'name' => 'Баланс',
				'id' => 13,
				'active' => true,
			),
			array(
				'name' => 'История покупок',
				'id' => 14,
				'active' => false,
			),
		);




		$data['list'] = $list;

		break;

	case 'getKitCollect':
		// запрос расположения и настроек экранов на главной странице
		
		$list = array(
			array(
				array(
					'id' => '1',
					'name' => 'company',
					'title' => 'О компании',
					'hide' => false
				),
				array(
					'id' => '2',
					'name' => 'adverts',
					'hide' => false,
					'title' => 'Объявления'
				),
				array(
					'id' => '3',
					'name' => 'articles',
					'hide' => false,
					'title' => 'Статьи'
				),
				array(
					'id' => '4',
					'name' => 'news',
					'hide' => false,
					'title' => 'Новости'
				),
				array(
					'id' => '5',
					'name' => 'blog',
					'title' => 'Блог',
					'hide' => false,
					'disable' => true
				),
				array(
					'id' => '6',
					'name' => 'followers',
					'hide' => false,
					'title' => 'Подписчики'
				),
				array(
					'id' => '7',
					'name' => 'follows',
					'hide' => false,
					'title' => 'Подписки'
				),
				array(
					'id' => '8',
					'name' => 'employees',
					'hide' => false,
					'title' => 'Мои сотрудники'
				),
			),
			array(
				array(
					'id' => '9',
					'name' => 'statistic',
					'hide' => false,
					'title' => 'Статистика'
				),
				array(
					'id' => '10',
					'name' => 'notifications',
					'hide' => false,
					'title' => 'Оповещения'
				),
				array(
					'id' => '12',
					'name' => 'balance',
					'hide' => false,
					'title' => 'Баланс'
				),
//				array(
//					'id' => '13',
//					'name' => 'history',
//					'hide' => false,
//					'title' => 'История покупок',
//				),
				array(
					'id' => '11',
					'name' => 'headings',
					'hide' => false,
					'title' => 'Заголовки'
				),
			)
		);




		$data['list'] = $list;

		break;

	case 'getSupportMenu':

		$data['list'] = array(
			array(
				'id' => '1',
				'name' => 'Использование Headerlink'
			),
			array(
				'id' => '2',
				'name' => 'Безопасность и вход'
			),
			array(
				'id' => '3',
				'name' => 'Конфидециальность'
			),
			array(
				'id' => '4',
				'name' => 'Настройка аккаунта'
			),
			array(
				'id' => '5',
				'name' => 'Еще какой-то раздел'
			),
			array(
				'id' => '6',
				'name' => 'Я не нашел ответа'
			)
		);

		break;

	case 'getSupportList':
		$data['supportMenuID'] = isset($_POST['id']) ? $_POST['id'] : '1';
		$data['head'] = array(
			'icon' => 'http://crm.headerlink.maestros.ru/public/img/svg/icon_support.svg',
			'title' => 'Использование Headerlink',
			'subtitle' => 'В этом разделе вы найдете ответы на более распространенные вопросы, возникающие у пользователей.'
		);

		$data['list'] = array(
			array(
				'id' => '1',
				'name' => 'Меня шантажируют!'
			),
			array(
				'id' => '2',
				'name' => 'Я удалил сообщения. Как восстановить?'
			),
			array(
				'id' => '3',
				'name' => 'Что делать, если я забыл пароль?'
			),
			array(
				'id' => '4',
				'name' => 'Почему у меня не работает приложение?'
			),
			array(
				'id' => '5',
				'name' => 'Как найти игры на сайте?'
			),
			array(
				'id' => '6',
				'name' => 'Как удалить страницу, к которой нет доступа?'
			),
			array(
				'id' => '7',
				'name' => 'Как убрать приложение?'
			),
			array(
				'id' => '8',
				'name' => 'Мой номер занят каким-то пользователем. Как мнепривязать его к своей странице?'
			),
			array(
				'id' => '9',
				'name' => 'Раздел «Рекомендации» в аудиозаписях. Популярное'
			),
			array(
				'id' => '10',
				'name' => 'Как создать игру или приложение?'
			),
			array(
				'id' => '11',
				'name' => 'Меня шантажируют!'
			),
			array(
				'id' => '12',
				'name' => 'Я удалил сообщения. Как восстановить?'
			),
			array(
				'id' => '13',
				'name' => 'Что делать, если я забыл пароль?'
			),
			array(
				'id' => '14',
				'name' => 'Почему у меня не работает приложение?'
			),
			array(
				'id' => '15',
				'name' => 'Как найти игры на сайте?'
			),
			array(
				'id' => '16',
				'name' => 'Как удалить страницу, к которой нет доступа?'
			),
			array(
				'id' => '17',
				'name' => 'Как убрать приложение?'
			),
			array(
				'id' => '18',
				'name' => 'Мой номер занят каким-то пользователем. Как мнепривязать его к своей странице?'
			),
			array(
				'id' => '19',
				'name' => 'Раздел «Рекомендации» в аудиозаписях. Популярное'
			),
			array(
				'id' => '20',
				'name' => 'Как создать игру или приложение?'
			),
		);
		break;

	case 'getSupportArticle':
		$data['supportMenuID'] = '2';

		$data['html'] =
			'<h1>Заголовок 1</h1>
			<h2>Заголовок 2</h2>
			<h3>Заголовок 3</h3>
			<h4>Заголовок 4</h4>
			<h5>Заголовок 5</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquam eleifend sem sit amet bibendum. In eleifend rutrum purus, nec commodo magna faucibus quis. Pellentesque dictum lacus dignissim magna condimentum, ut rhoncus eros molestie. Quisque sodales accumsan nisl, non dictum dui lobortis ac. Sed venenatis purus vitae convallis pretium. Vestibulum metus sapien, luctus eget risus ac, elementum ornare nunc. Praesent consectetur, mi commodo imperdiet commodo, augue purus consequat lectus, in porttitor lorem mi ac lectus. Donec pellentesque ex in mauris mollis hendrerit. Vestibulum ac ex sed ipsum varius molestie vitae non nibh. Nam cursus, nibh rhoncus efficitur fringilla, augue lacus placerat est, sed venenatis purus libero a nunc. Quisque ultrices fringilla ultrices. In ultricies tortor non leo viverra blandit. Vestibulum et velit eget quam interdum euismod sed et nunc. Maecenas elementum quis mi in laoreet. Integer sed aliquet nibh, ut sagittis lacus. Donec eget tincidunt dui, ut lacinia justo.</p>
			<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam auctor luctus nisi. Duis sollicitudin diam quis interdum sodales. Pellentesque iaculis nibh eget metus tincidunt, vel vestibulum velit lobortis. Sed posuere sollicitudin vulputate. Quisque porttitor, risus eget dignissim pretium, dolor nunc pretium orci, nec luctus velit odio in leo. Donec finibus libero quis orci iaculis ultricies. Cras a lorem sed erat sagittis vulputate. Mauris sagittis libero et enim bibendum varius. Curabitur facilisis nec mauris vel varius. Praesent a porta magna. Phasellus vulputate cursus tortor id pulvinar. In ultricies sed ipsum id iaculis. Sed dictum odio non cursus mattis. Vestibulum mauris nunc, mollis vitae augue ut, tempus sollicitudin augue. Duis egestas pharetra velit, eu tincidunt libero varius vitae.</p>
			<p>Morbi convallis at felis ac fringilla. Donec iaculis felis nec nibh viverra finibus. Integer nec bibendum lorem, feugiat facilisis diam. Nulla sagittis tristique finibus. Phasellus porttitor diam at magna facilisis, ut eleifend arcu maximus. Aliquam maximus commodo consectetur. Aliquam ac erat ut lorem tincidunt elementum.</p>
			<ul>
			<li>Список</li>
			<li>Список</li>
			<li>Список</li>
			<li>Список</li>
			</ul>
			<p>Sed quis pharetra odio, et efficitur dui. Ut maximus tortor velit, vel tincidunt lorem interdum vitae. Morbi imperdiet pretium ex at molestie. Aenean scelerisque ipsum et turpis tincidunt pulvinar. Ut ac tristique elit. Sed tempor purus quam, vel varius ex auctor eget. Sed blandit suscipit tincidunt. Praesent ornare facilisis ultrices. Suspendisse ac interdum eros. Nam imperdiet nibh et turpis suscipit, vitae tincidunt elit fermentum. Duis fermentum et ipsum vitae sagittis. Donec lacinia arcu est, nec efficitur ipsum volutpat in. Nam congue eget ipsum eu pulvinar. Sed commodo vitae enim id placerat. Suspendisse ultricies dolor quis nibh rhoncus, quis suscipit tellus elementum.</p>
			<p>Duis consectetur ex nec nisi malesuada, efficitur ultricies augue tempus. Etiam consectetur, eros at luctus consectetur, dolor nulla fermentum magna, non pulvinar massa urna non nibh. Cras in urna fringilla, consequat lacus in, interdum arcu. Nulla facilisi. Fusce non lacus sed turpis vehicula cursus. Nam pellentesque arcu neque, eget tempor ante molestie vestibulum. Donec suscipit ultrices tortor ut ultrices. Vestibulum id congue ex. Phasellus a ullamcorper massa.</p>';

		break;

	case 'getStatistic':

		$stat = array();
		$now = round(microtime(true) * 1000);
		$count = 0;

		for ($i = 0; $i < 500; $i++) {


			array_push($stat, array(
				"x" => $now - ((500 - $i) * 86400000),
				"y" => $count
			));

			$count = $count + rand(-20, 20);

			if ( $count < 0) $count = -$count;

		}

		$data['data'] = $stat;	// массив статистики
		$data['rate'] = 890;	// общий рейтинг
		$data['message'] = '';	// сообщение, например если статистики нет "заголовок не выбран.
		$data['objectId'] = 1856;	// id объекта (например заголовка) по которомы выдан рейтинг

		break;

	case 'setNotificationSetting':	//
	case 'setKitSetting':	// сохранение настроек главной страницы
	case 'setKitCollect':	// сохранение экранов главной страницы

		$data['status'] = true;

		break;

	case 'getKitNews':	// запрос для блока количества новостей на главном экране
		$data['all'] = 0;
		$data['active'] = 0;
		break;

	case 'getKitArticles':	// запрос для блока количества статей на главном экране
		$data['all'] = 0;
		$data['active'] = 0;
		break;

	case 'getKitAdverts':	// запрос для блока количества объявлений на главном экране
		$data['all'] = 45065;
		$data['active'] = 38560;
		break;

	case 'getKitNotifications':	// запрос для блока оповещения на главном экране
		$notifications = array();

		for ($i = 0; $i < 42; $i++) {

			array_push($notifications, array(
				"id" => $i + 1,
				"info" => array(
					"photo" => '/img/dev/logo_'.rand(0, 5).'.jpg',
					"title" => "Заголовок",
					"date" => randomDate('2017-12-10', time()),
				)
			));

		}
		$data['list'] = $notifications;

		break;

	case 'getKitFollowers':	// запрос для блока подписчиков на главном экране
		$followers = array();

		for ($i = 0; $i < 42; $i++) {

			array_push($followers, array(
				"photo" => '/img/dev/logo_'.rand(0, 5).'.jpg',
				"online" => rand(0, 1) == 1
			));

		}
		$data['list'] = $followers;

		break;

	case 'getKitFollows':	// запрос для блока подписки на главном экране

		$data['list'] = [];

		break;

	case 'deleteDialog':
		break;
		
	case 'insertBranch':
		$data = api($_POST);
		break;
		
	case 'deleteBranch':
		$data = api($_POST);
		break;
		
	case 'addCompany':
		$data = api($_POST);
		/*session_start();
		$request = $_POST;
		$request = http_build_query($request); 
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"http://lk.headerlink.ru/api/");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		session_write_close();
		$json = curl_exec($ch);
		$data = json_decode($json);

		curl_close ($ch);*/
		break;
		
	case 'loadAdvert':
		$data = api($_POST);
		/*$data['advert_title'] = 'Какйо то там заголовок';
		$data['advert_theme'] = array(522);
		$data['advert_text'] = 'вф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы ввф фвывфыв фыв фывфы фывфывф фвфывф ыфв фыв фывфы в';
		$data['advert_link'] = 'https://sonata-project.org/bundles/admin/3-x/doc/index.html';
		$data['advert_price'] = '';
		$data['advert_amount'] = 2;
		$data['advert_branch'] = array(1, 3, 4);
		$data['advert_gallery'] = array(
			array(
				"comment" => '',
				"id" => 5984,
				"rotate" => 0,
				"extension" => 'jpg',
				"thumbnail" => 'https://pp.userapi.com/c850532/v850532402/14557f/MXhhbufHfOM.jpg'
			)
		);
		$data['advert_type'] = 2;*/
		break;

	case 'loadBranch':
		$data = api($_POST);
		break;
		/*$data['branch_title'] = 'Название филиала';
		$data['branch_type'] = 2;
		$data['address_all'] = 'Россия, Санкт-Петербург, Невский проспект, 160';
		$data['address_country'] = 'Россия Санкт-Петербург';
		$data['address_city'] = 'Санкт-Петербург';
		$data['address_street'] = 'Невский проспект';
		$data['address_house'] = '16';
		$data['address_part'] = '1';
		$data['address_lit'] = 'F';
		$data['address_room'] = '1';
		$data['branch_metro'] = array(40, 52, 54);
		$data['phone_number'] = array('7 123 456 78 99', '7 654 456 87 99', ' 7 987 132 16 54');
		$data['branch_email'] = 'test@test.test';
		$data['branch_site'] = 'test.test';

		break;*/

	case 'getDialog':

		$dialogs = array();
		$leng = rand(1, 50);

		if ( $leng > $_POST['count']) $leng = $_POST['count'];

		for ($i = 0; $i < $leng ; $i++) {

			$user = getUserLogo(rand(0, 5));
			$text = 'Nulla rutrum arcu et eros maximus, eu lobortis quam hendrerit. Vivamus molestie eget nibh sit amet vehicula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse tempor vitae arcu a pulvinar. Vivamus tincidunt ipsum diam, id iaculis erat bibendum nec. In nec efficitur ipsum, in pellentesque urna. Aliquam a dignissim mauris, ut tempus risus. Maecenas hendrerit nisi vitae elementum congue. Proin viverra mi ac nisi eleifend, id scelerisque ipsum posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis quis turpis vitae dui congue placerat eu nec tellus. Integer convallis gravida tortor quis rhoncus. Sed quam nisl, blandit et neque eget, pharetra lobortis orci. Nulla aliquet massa sem, nec vestibulum velit pretium eu. Sed dapibus mauris dui, a bibendum odio aliquet id. Nullam ligula metus, feugiat at commodo id, consectetur in justo';

			$message = substr($text, rand(0, 230), rand(250, 500));

			$isFile = rand(0,3) == 1;
			$isAttach = rand(0,4) == 1;

			array_push($dialogs, array(
				"id" => $_POST['id'] . ($i + 1 + $_POST['offset']),
				"userId" => $user['id'],
				"logo" => $user['logo'],
				"name" => $user['name'],
				"date" => date('Y-m-d', (time()) - (($i + $_POST['offset']) * 7200)),
				"online" => rand(0, 2) == 1,
				"message" => ucfirst($message),
				"attach" => $isAttach && !$isFile ? array(
					array(
						"id" => $_POST['id'] . ($i + 1),
						"userId" => $user['id'],
						"logo" => $user['logo'],
						"name" => $user['name'],
						"date" => date('Y-m-d', (time()) - (($i + $_POST['offset']) * 7200)),
						"online" => rand(0, 2) == 1,
						"message" => ucfirst(substr($message, rand(0, 40), rand(50, 100))),
					),
					array(
						"id" => $_POST['id'] . ($i + 1),
						"userId" => $user['id'],
						"logo" => $user['logo'],
						"name" => $user['name'],
						"date" => date('Y-m-d', (time()) - (($i + $_POST['offset']) * 7200)),
						"online" => rand(0, 2) == 1,
						"message" => ucfirst(substr($message, rand(0, 40), rand(50, 100))),
					),
					array(
						"id" => $_POST['id'] . ($i + 1),
						"userId" => $user['id'],
						"logo" => $user['logo'],
						"name" => $user['name'],
						"date" => date('Y-m-d', (time()) - (($i + $_POST['offset']) * 7200)),
						"online" => rand(0, 2) == 1,
						"message" => ucfirst(substr($message, rand(0, 40), rand(50, 100))),
					),
				) : false,
				"files" => $isFile ? array(
					array(
						"name" => "Название файла.jpeg" ,
						"type" => "jpeg",
						"thumbnail" => "http://crm.headerlink.maestros.ru/img/dev/picture_1.jpg",
						"link" => ""
					),
					array(
						"name" => "Название файла.docx" ,
						"type" => "docx",
						"thumbnail" => "",
						"link" => ""
					),
					array(
						"name" => "Название файла.pdf" ,
						"type" => "pdf",
						"thumbnail" => "",
						"link" => ""
					),
					array(
						"name" => "Название файла.jpeg" ,
						"type" => "jpeg",
						"thumbnail" => "http://crm.headerlink.maestros.ru/img/dev/picture_1.jpg",
						"link" => ""
					),
					array(
						"name" => "Название файла.docx" ,
						"type" => "docx",
						"thumbnail" => "",
						"link" => ""
					),
					array(
						"name" => "Название файла.pdf" ,
						"type" => "pdf",
						"thumbnail" => "",
						"link" => ""
					)
				) : false,
				"filesArchive" => $isFile ? '/архив_с_файлами.zip' : false,
			));

		}


		$data['name'] = "Диалог № ".$_POST['id'];
		$data['allowEdit'] = rand(0,1) == 1;	// если это создтель диалога
		$data['disabled'] = rand(0,2) == 1; // если диалог закрыт
		$data['message'] = $data['disabled'] ? (rand(0,1) == 1 ? 'Диалог перенесен в архив' : 'Вас удалили, вы не можете писать' ) : ''; // статус
		$data['list'] = $dialogs;

		break;


	case 'getNewDialogUsersList':

		$users = array();
		$leng = rand(10, 50);

		for ($i = 0; $i < $leng ; $i++) {

			$user = getUserLogo(rand(0, 5));

			array_push($users, array(
				"id" => $offset + $i,
				"name" => $user['name'],
				"logo" => $user['logo'],
			));

		}

		$data['list'] = $users; // если контактов еще не добавили то присылвать значение false, если поиск не дал результатов, то пустой массив.

		break;

	case 'getDialogUsersList':

		$users = array();
		$leng = rand(10, 50);

		for ($i = 0; $i < $leng ; $i++) {

			$user = getUserLogo(rand(0, 5));

			array_push($users, array(
				"hold" => $i == 0,
				"status" => $i == 0 ? 'Создатель беседы' : '',
				"id" => $offset + $i,
				"name" => $user['name'],
				"logo" => $user['logo'],
			));

		}

		$data['list'] = $users; // если контактов еще не добавили то присылвать значение false, если поиск не дал результатов, то пустой массив.

		break;
}
if(count($data)) {
	//header("Location: http://lk.headerlink.ru/api/?action=".$action);
	//exit;
	//print_r($data); exit;
}
echo json_encode($data);
?>